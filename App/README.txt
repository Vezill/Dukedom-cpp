QDukedom installation guide:

To build and run QDukedom from source you will require a C++ compiler, the Qt Framework 4.8 and
Qt Creator. Once both are installed, just open up the project file (QDukedom.pro) in Qt Creator, it
should ask you what kind of build kit you want to use (it should auto detect this and provide some
options to choose from), then just run the project (Ctrl+R, Build -> Run or the green play arrow).

Here are links to installers required to build the project on Windows:
MinGW: http://sourceforge.net/projects/mingw/files/
Qt 4.8: http://qt-project.org/downloads (scroll down to 4.8, get the MinGW one)
Qt Creator: http://qt-project.org/downloads#qt-creator

Note: This application SHOULD be easily upgraded to Qt 5+, you would just have to switch any of the 
include that refer to widgets (most UI elements) to <QtWidgets/ instead of <QtGui/
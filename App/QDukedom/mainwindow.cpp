/* mainwindow.cpp
 *
 * Dukedom Game Class
 * Version: 0.4
 * Date: November 20, 2013
 * Author: Nick Donais
 * Email: dona0078@algonquinlive.com
 * SID: 040596989
 * Course: CST8333 Advanced Languages
 * Lab Section: 410
 * Professor: Stan Pieda
 *
 * This is the main window setup for QDukedom. Although this was the original plan, this was
 * abandoned half way through when I realized just how much of a code rewite I'd have to do to the
 * Dukedom class to get this to work the way I wanted that I decided to do something else for the
 * requirements.
 */
#include <QtGui/QPixmap>
#include <QtGui/QPushButton>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>

#include "mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent) {
    QPixmap newPixmap(":/icons/plus-circle.png");
    QPixmap quitPixmap(":/icons/cross-circle.png");
    QPixmap aboutPixmap(":/icons/information.png");

    // Setup the toolbar
    QToolBar *uiToolBar = addToolBar("Main ToolBar");
    uiToolBar->addAction(QIcon(newPixmap), "New Game", this, SLOT(reset()));
    uiToolBar->addAction(QIcon(quitPixmap), "Quit", qApp, SLOT(quit()));
    uiToolBar->addAction(QIcon(aboutPixmap), "About Qt", qApp, SLOT(aboutQt()));

    // Initialize saved widgets
    uiYearLabel = new QLabel("Year 0 Data");
    uiYearLabel->setAlignment(Qt::AlignCenter);

    uiYearDataTextEdit = new QTextEdit();
    uiYearDataTextEdit->setReadOnly(true);

    uiStageDescLabel = new QLabel("New Game");
    uiStageDescLabel->setAlignment(Qt::AlignCenter);

    uiStageControlsWidget = new QWidget();

    // Setup feed peasants stage layout
    uiGrainAmountLineEdit = new QLineEdit();
    uiGrainAmountLineEdit->setInputMask("000000");
    uiGrainAmountLineEdit->setMaxLength(6);

    QPushButton *uiFeedPeasantsButton = new QPushButton("Submit");
    connect(uiFeedPeasantsButton, SIGNAL(clicked()), this, SLOT(handleFeedPeasants()));

    uiFeedPeasantsLayout = new QHBoxLayout();
    uiFeedPeasantsLayout->addWidget(uiGrainAmountLineEdit);
    uiFeedPeasantsLayout->addWidget(uiFeedPeasantsButton);
    uiFeedPeasantsLayout->setStretch(0, 2);
    uiFeedPeasantsLayout->setStretch(1, 0);

    // Setup buy/sell land stage layout
    uiBuyLandRadioButton = new QRadioButton("Buy");
    QRadioButton *uiSellLandRadioButton = new QRadioButton("Sell");

    uiLandAmountLineEdit = new QLineEdit();
    uiLandAmountLineEdit->setInputMask("000000");
    uiLandAmountLineEdit->setMaxLength(6);

    QPushButton *uiLandButton = new QPushButton("Submit");
    connect(uiLandButton, SIGNAL(clicked()), this, SLOT(handleBuySellLAnd()));

    uiLandLayout = new QGridLayout();
    uiLandLayout->addWidget(uiBuyLandRadioButton, 0, 0, 1, 1);
    uiLandLayout->addWidget(uiSellLandRadioButton, 0, 1, 1, 1);
    uiLandLayout->addWidget(uiLandAmountLineEdit, 1, 0, 1, 2);
    uiLandLayout->addWidget(uiLandButton, 0, 2, 2, 1);

    // Setup war stage layout
    uiMercAmountLineEdit = new QLineEdit();
    uiMercAmountLineEdit->setInputMask("000000");
    uiMercAmountLineEdit->setMaxLength(6);

    uiAttackRadioButton = new QRadioButton("Attack");
    connect(uiAttackRadioButton, SIGNAL(toggled(bool)),
            uiMercAmountLineEdit, SLOT(setEnabled(bool)));
    QRadioButton *uiWaitRadioButton = new QRadioButton("Wait");

    QPushButton *uiWarButton = new QPushButton("Submit");
    connect(uiWarButton, SIGNAL(clicked()), this, SLOT(handleWar()));

    uiWarLayout = new QGridLayout();
    uiWarLayout->addWidget(uiAttackRadioButton, 0, 0, 1, 1);
    uiWarLayout->addWidget(uiWaitRadioButton, 0, 1, 1, 1);
    uiWarLayout->addWidget(uiMercAmountLineEdit, 1, 0, 1, 2);
    uiWarLayout->addWidget(uiWarButton, 0, 2, 2, 1);

    // Setup year end stage layout
    QPushButton *uiEndTurnButton = new QPushButton("End Turn");
    connect(uiEndTurnButton, SIGNAL(clicked()), this, SLOT(handleYearEnd()));

    uiYearEndLayout = new QHBoxLayout();
    uiYearEndLayout->addWidget(uiEndTurnButton);
    uiYearEndLayout->setContentsMargins(10, 10, 10, 10);

    // Setup the main layout
    QVBoxLayout *uiMainLayout = new QVBoxLayout();
    uiMainLayout->addWidget(uiYearLabel);
    uiMainLayout->addWidget(uiYearDataTextEdit);
    uiMainLayout->addWidget(uiStageDescLabel);
    uiMainLayout->addWidget(uiStageControlsWidget);
    uiMainLayout->setStretch(0, 0);
    uiMainLayout->setStretch(1, 6);
    uiMainLayout->setStretch(2, 1);
    uiMainLayout->setStretch(3, 1);
}

void MainWindow::reset() {

}

void MainWindow::handleFeedPeasants() {

}

void MainWindow::handleBuySellLand() {

}

void MainWindow::handleWar() {

}

void MainWindow::handleYearEnd() {

}

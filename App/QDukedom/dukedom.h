/* dukedom.h
 *
 * Dukedom Game Class
 * Version: 0.5
 * Date: October 17, 2013
 * Author: Nick Donais
 * Email: dona0078@algonquinlive.com
 * SID: 040596989
 * Course: CST8333 Advanced Languages
 * Lab Section: 410
 * Professor: Stan Pieda
 *
 * Please note that this code is freely available in both C and Java at the following github:
 * https://github.com/caryo/Dukedom. This code is a translation into C++/Qt of my Java recode of the
 * original Java code for my Graphics course last year. The goal is to eventually turn this into
 * an object that a Qt GUI will pull data from to have a graphical version instead of just a cli.
 */
#ifndef DUKEDOM_H
#define DUKEDOM_H

#include <QtCore/QTime>
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtCore/QStringList>


class Dukedom {
    /* Constants */
    static const int MAX_PEASANTS = 8;
    static const int MAX_LAND = 3;
    static const int MAX_GRAIN = 10;
    static const int MAX_FERTILITY = 6;
    static const int RANDOM_RANGE = 2;
    static const double INITIAL_CROP_YIELD = 3.95;
    static const double WAR_FACTOR = 1.95;
    static const double MAX_RANDOM_VALUES = 8;

    /* Variables */
    /* Controls if detailed year reports are printed. */
    bool mSkipDetailedReports;
    /* Total amount of peasants. */
    int mTotalPeasants;
    /* Total amount of land. */
    int mTotalLand;
    /* Total amount of grain. */
    int mTotalGrain;
    /* Current year value. */
    int mCurrentYear;
    /* Crop yield for the current year. */
    double mCropYield;
    /* Controls disaster chance. */
    int mDisaster;
    /* First of two unrest tracking values. */
    int mUnrest1;
    /* Second unrest tracking value. */
    int mUnrest2;
    /* Tracks the kings likelyhood of war status towards the player */
    int mWarWithKingStatus;
    /* Container for yearly peasants alterations. */
    QList<int> mPeasants;
    /* Container for yearly land alterations. */
    QList<int> mLand;
    /* Container for yearly grain alterations. */
    QList<int> mGrain;
    /* Container for yearly land fertility alterations. */
    QList<int> mLandFertility;
    /* Contrainer for random value generation. */
    QList<int> mRandomValue;
    /* Contains text to be printed related to peasants. */
    QStringList mPeasantsText;
    /* Contains text to be printed related to land. */
    QStringList mLandText;
    /* Contrans text to be printed related to grain. */
    QStringList mGrainText;

public:
    /**
     * Dukedom constructor, simple variable initialization and expands the QLists into the size
     * required for variables. Normally you wouldn't handle QLists this way, but I'm trying to
     * follow the original placements for data, since the QLists hold yearly records data basically
     * for the year.
     */
    Dukedom();

    /**
     * Asks the user if they want to buy or sell land and handles calculations for fertile land and
     * the kings land tax.
     */
    void buySellLand();

    /**
     * Display saved high scores from file.
     */
    void displayScores();

    /**
     * Checks all game over conditions and returns a boolean based on if the game will continue or
     * not.
     * @return boolean game over state
     */
    bool endOfGameCondition();

    /**
     * Asks the user how much grain they want to spend to feed the current amount of peasants in
     * their kingdom and returns a game over condition if too many are unfed.
     * @return boolean gave over condition
     */
    bool feedPeasants();

    /**
     * The main game loop for a cli.
     */
    void gameLoop();

    /**
     * Handles asking the user how much grain they want to plant based on how much grain they have
     * left and how much land they have. Also handles the kings taxes.
     */
    void grainProduction();

    /**
     * Sets up all variables for a new game.
     */
    void initializeVars();

    /**
     * Checks the the user can purchase land or plant farms with their current amount of grain.
     * @param purchasePrice the grain purchase price for new land or planted land
     */
    void insufficientGrain(int purchasePrice) const;

    /**
     * The main structure of a year of gameplay. Calls each method in proper order and returns the
     * state of the current game.
     * @return boolean game state
     */
    bool playOneYear();

    /**
     * Prints the data corresponding to the text in mGrainText for the current year.
     */
    void printGrain();

    /**
     * Prints the data corresponding to the text in mLandText for the current year.
     */
    void printLand();

    /**
     * Prints last years data for the user to make decisions for the new year.
     */
    void printLastYearsResults();

    /**
     * Prints the data corresponding to the text in mPeasantText for the current year.
     */
    void printPeasants();

    /**
     * Prints the current years totals for the users land and peasants.
     */
    void printTotals() const;

    /**
     * Prints testing variables. Not used but kept around because it's in the orignal code.
     */
    void printVars() const;

    /**
     * Resets all yearly record variables for a new year.
     */
    void resetYearlyStats();

    /**
     * Save end game stats to a file.
     * @param type tells the method to record a win or loss
     */
    void saveScore();

    /**
     * Checks the current amount of grain left for a game over condition.
     * @return boolean game over condition
     */
    bool totalGrainCheck() const;

    /**
     * Checks the current amount of land left for a game over condition.
     * @return boolean game over condition
     */
    bool totalLandCheck() const;

    /**
     * Checks the current peasant count for a game over condition.
     * @return boolean game over condition
     */
    bool totalPeasantCheck() const;

    /**
     * Checks current unrest values for a game over condition.
     * @return boolean game over condition
     */
    bool unrestCheck() const;

    /**
     * Handles grain calculations for the end of the year depending on grain planted and decay.
     */
    void updateGrainTotal();

    /**
     * Updates the current population at the end of a year and handles distaster states.
     */
    void updatePopulation();

    /**
     * Handles royal tax at the end of each year and returns a game over state if the user doesn't have
     * enough grain to pay.
     * @return boolean game over state
     */
    bool updateRoyalTax();

    /**
     * Updates the current war state with the king and returns a war status as the user can only be
     * at war with the king or it's neighbours, not both.
     * @return boolean war status
     */
    bool updateWarStatus();

    /**
     * Handles direct war with the king and returns a game over condition depending on how it ends.
     * @return boolean game over condition
     */
    bool warWithKing() const;

    /**
     * Handles both the kings suspicions and war depending on the current war state and the users
     * grain wealth. This will return a game over condition.
     * @return boolean game over condition
     */
    bool warWithNeighbour();

private:
    /**
     * Formats a QString based on values given.
     * @param intVal value to format
     * @param fieldSize amount of spaces to format with
     * @param rightJustify if the text should be right justified
     * @return formatted QString
     */
    QString formatInteger(int intVal, int fieldSize, bool rightJustify);

    /**
     * Formats a QString based on string given.
     * @param strVal string to format
     * @param fieldSize amount of spaces to format with
     * @return  formatted QString
     */
    QString formatString(QString strVal, int fieldSize);

    /**
     * Get an integer from the user. This is only for use with a CLI.
     * @return integer from user
     */
    int getInteger();

    /**
     * Generate a random value based on the value in mRandomValue at the given position.
     * @param pos position in mRandomValue
     * @return random value
     */
    int getRandomNumber(int pos);

    /**
     * Generate a random value between given min and max values/
     * @param min minimum value
     * @param max maximum value
     * @return random value
     */
    int getRandomNumber(int min, int max);

    /**
     * Prompt the user for a yes or no answer. This is only for use with a CLI.
     * @return string from user
     */
    bool getYesNo();

    /**
     * Generate a random with certain conditions applied to it.
     * @param min minimum value
     * @param max maximum value
     * @return random value
     */
    int initRandomNumber(int min, int max);
};

#endif  // DUKEDOM_H

/* dukedom.cpp
 *
 * Dukedom Game Class
 * Version: 1.0
 * Date: October 17, 2013
 * Author: Nick Donais
 * Email: dona0078@algonquinlive.com
 * SID: 040596989
 * Course: CST8333 Advanced Languages
 * Lab Section: 410
 * Professor: Stan Pieda
 *
 * Please note that this code is freely available in both C and Java at the following github:
 * https://github.com/caryo/Dukedom. This code is a translation into C++/Qt of my Java recode of the
 * original Java code for my Graphics course last year. The goal is to eventually turn this into
 * an object that a Qt GUI will pull data from to have a graphical version instead of just a cli.
 */
#include <QtCore/QFile>
#include <QtCore/QTextStream>

#include "dukedom.h"


QTextStream qtout(stdout);
QTextStream qtin(stdin);


/**
 * Dukedom constructor, simple variable initialization and expands the QLists into the size
 * required for variables. Normally you wouldn't handle QLists this way, but I'm trying to follow
 * the original placements for data, since the QLists hold yearly records data basically for the
 * year.
 */
Dukedom::Dukedom()
    : mSkipDetailedReports(false), mTotalPeasants(0), mTotalLand(0), mCurrentYear(0),
      mCropYield(0.0), mDisaster (0), mUnrest1(0), mUnrest2(0), mWarWithKingStatus(0) {

    for (int i = 0; i < MAX_PEASANTS; ++i)
        mPeasants.append(0);

    for (int i = 0; i < MAX_LAND; ++i)
        mLand.append(0);

    for (int i = 0; i < MAX_GRAIN; ++i)
        mGrain.append(0);

    for (int i = 0; i < MAX_FERTILITY; ++i)
        mLandFertility.append(0);

    mPeasantsText << "Peasants at start "
                  << "Starvation        "
                  << "King's Levy       "
                  << "War casualties    "
                  << "Looting victims   "
                  << "Disease victims   "
                  << "Natural deaths    "
                  << "Births            ";

    mLandText << "Land at start     "
              << "Bought/sold       "
              << "Fruits of war     ";

    mGrainText << "Grain at start    "
               << "Used for food     "
               << "Land deals        "
               << "Seeding           "
               << "Rat losses        "
               << "Mercenary hire    "
               << "Fruits of war     "
               << "Crop yield        "
               << "Castle expense    "
               << "Royal tax         ";

    for (int i = 0; i < MAX_RANDOM_VALUES; ++i)
        mRandomValue.append(0);
}


/* ********** Public Methods ********** */

/**
 * Asks the user if they want to buy or sell land and handles calculations for fertile land and the
 * kings land tax.
 */
void Dukedom::buySellLand() {
    double tempFactor = mCropYield;
    int randomOne = getRandomNumber(1);
    int purchasePrice = static_cast<int>((2 * tempFactor) + static_cast<double>(randomOne) - 5.0);
    int landToBuy = 0;
    int grainForPurchase = 0;
    bool validInput = false;

    if (purchasePrice < 4)
        purchasePrice = 4;

    while (!validInput) {
        qtout << "Land to buy at " << purchasePrice << " HL./HA. = " << flush;
        landToBuy = getInteger();

        grainForPurchase = landToBuy * purchasePrice;
        if (grainForPurchase > mTotalGrain) {
            insufficientGrain(purchasePrice);
            continue;
        }

        validInput = true;
    }

    if (landToBuy > 0) {
        mGrain[2] = grainForPurchase * -1;
        mLand[1] = landToBuy;
        mLandFertility[2] += landToBuy;
        mTotalGrain -= grainForPurchase;
        mTotalLand += landToBuy;
    }
    else {
        int landAvailableToSell = mLandFertility[0] + mLandFertility[1] + mLandFertility[2];
        int sellingPrice = purchasePrice - 1;
        int landToSell = 0;
        int grainForLandSale = 0;

        validInput = false;

        for (int i = 0; (i < 3) && (!validInput); --sellingPrice, ++i) {
            qtout << "Land to sell at " << sellingPrice << " HL./HA. = " << flush;
            landToSell = getInteger();

            if (landToSell > landAvailableToSell) {
                qtout << "But you only have " << landAvailableToSell << " HA. of good land" << endl;
                continue;
            }

            grainForLandSale = landToSell * sellingPrice;
            if (grainForLandSale > 4000)
                qtout << "No buyers have that much grain, try less" << endl;
            else
                validInput = true;
        }

        if (!validInput)
            qtout << "Buyers have lost interest" << endl;
        else {
            if (landToSell > 0) {
                mLand[1] = landToSell * -1;

                int tempLandToSell = landToSell;
                int fertilityIndex = 2;

                for (int i = 0; i < 3; ++i) {
                    if (tempLandToSell <= mLandFertility[fertilityIndex])
                        break;
                    else {
                        tempLandToSell -= mLandFertility[fertilityIndex];
                        mLandFertility[fertilityIndex] = 0;
                        fertilityIndex -= 1;
                    }
                }

                mLandFertility[fertilityIndex] -= tempLandToSell;
                mTotalLand -= landToSell;

                if (sellingPrice < 4) {
                    qtout << "The High King appropriates half\n"
                          << "of your earnings as punishment\n"
                          << "for selling at such low price" << endl;

                    grainForLandSale /= 2;
                }

                mTotalGrain += grainForLandSale;
                mGrain[2] = grainForLandSale;
            }
        }
    }
}


/**
 * Display saved high scores from file.
 */
void Dukedom::displayScores() {
    QFile scoresFile("dukedom_scores.txt");
    if (!scoresFile.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    qtout << "Current scores list: " << endl;

    QTextStream in(&scoresFile);
    while (!in.atEnd()) {
        QString score = in.readLine();
        qtout << score << endl;
    }

    qtout << endl;
}


/**
 * Checks all game over conditions and returns a boolean based on if the game will continue or not.
 * @return boolean game over state
 */
bool Dukedom::endOfGameCondition() {
    // Too few peasants
    if (totalPeasantCheck())
        return true;

    // Too little land left
    if (totalLandCheck())
        return true;

    // High disproval rating
    if (unrestCheck())
        return true;

    // Too little grain left
    if (totalGrainCheck())
        return true;

    // Reached age of retirement
    if (mCurrentYear > 45 && mWarWithKingStatus == 0) {
        qtout << "You have reached the age of retirement" << endl;
        return true;
    }

    mUnrest1 = 0;
    if (mWarWithKingStatus > 0) {
        mWarWithKingStatus = 2;

        qtout << "The king demands twice the royal tax in\n"
              << "THE HOPE THAT YOU WILL PROVOKE WAR. YOU PAY? " << flush;

        if (!getYesNo())
            mWarWithKingStatus = -1;
    }

    return false;
}


/**
 * Asks the user how much grain they want to spend to feed the current amount of peasants in their
 * kingdom and returns a game over condition if too many are unfed.
 * @return boolean gave over condition
 */
bool Dukedom::feedPeasants() {
    int grainForFood = 0;

    while (true) {
        qtout << "Grain for food = " << flush;
        grainForFood = getInteger();

        if (grainForFood < 100)
            grainForFood *= mTotalPeasants;

        if (grainForFood > mTotalGrain) {
            insufficientGrain(0);
            continue;
        }

        if ((grainForFood / mTotalPeasants) < 11 && grainForFood != mTotalGrain) {
            qtout << "Some peasants demonstrate before the\n"
                  << "castle with sharpened scythes" << endl;

            mUnrest1 += 3;
            continue;
        }

        break;
    }

    mGrain[1] = grainForFood * -1;
    mTotalGrain -= grainForFood;

    int grainPerPeasant = grainForFood / mTotalPeasants;

    if (grainPerPeasant < 13) {
        qtout << "Some peasants have starved" << endl;

        int peasantsFed = grainForFood / 13;
        int peasantsStarved = mTotalPeasants - peasantsFed;

        mPeasants[1] = peasantsStarved * -1;
        mTotalPeasants -= peasantsStarved;
    }

    int extraGrainPerPeasant = grainPerPeasant - 14;
    if (extraGrainPerPeasant > 4)
        extraGrainPerPeasant = 4;

    mUnrest1 += (3 * mPeasants[3]) - (2 * extraGrainPerPeasant);
    if (mUnrest1 > 88) {
        qtout << "The peasants tire of war and starvation\n"
              << "You are deposed" << endl;
        return true;
    }

    return (mTotalPeasants < 33);
}


/**
 * The main game loop for a cli.
 */
void Dukedom::gameLoop() {
    bool exitGame = false;

    qtout << "\nD U K E D O M\n"
          << "Do you want to skip detailed reports ? " << flush;
    if (getYesNo()) {
        mSkipDetailedReports = true;
        qtout << "\nSkipping detailed reports ..." << endl;
    }
    else
        mSkipDetailedReports = false;

    while (!exitGame) {
        initializeVars();

        qtout << "Show previous scores ? " << flush;
        if (getYesNo())
            displayScores();

        qtout << "Start a New Game ? " << flush;
        if (!getYesNo()) {
            exitGame = true;
            continue;
        }

        while(!playOneYear());

        qtout << "\nGame Over" << endl;
        saveScore();
    }
}


/**
 * Handles asking the user how much grain they want to plant based on how much grain they have left
 * and how much land they have. Also handles the kings taxes.
 */
void Dukedom::grainProduction() {
    int landToPlant = 0;
    int grainForSeeding = 0;
    bool exitLoop = false;

    while (!exitLoop) {
        qtout << "Land to be planted = " << flush;
        landToPlant = getInteger();

        if (landToPlant > mTotalLand) {
            qtout << "But you don't have enough land\n"
                  << "You only have " << mTotalLand << " HA. of land left" << endl;
            continue;
        }

        if (landToPlant > (4 * mTotalPeasants)) {
            qtout << "But you don't have enough peasants\n"
                  << "Your peasants can only plant " << (4 * mTotalPeasants) << " HA. of land"
                  << endl;
            continue;
        }

        grainForSeeding = 2 * landToPlant;
        if (grainForSeeding > mTotalGrain) {
            insufficientGrain(1);
            continue;
        }

        exitLoop = true;
    }

    QList<int> landPlanted;
    for (int i = 0; i < MAX_FERTILITY; ++i)
        landPlanted.append(0);

    if (landToPlant > 0) {
        mGrain[3] = grainForSeeding * -1;
        mTotalGrain -= grainForSeeding;
        mGrain[7] = landToPlant;

        int tempLandToPlant = landToPlant;
        int cropIndex = 0;

        for (int i = 0; i < 6; ++i) {
            if (tempLandToPlant <= mLandFertility[cropIndex]) {
                break;
            }

            tempLandToPlant -= mLandFertility[cropIndex];
            landPlanted[cropIndex] = mLandFertility[cropIndex];
            mLandFertility[cropIndex] = 0;
            ++cropIndex;
        }

        landPlanted[cropIndex] = tempLandToPlant;
        mLandFertility[cropIndex] -= tempLandToPlant;
    }

    mLandFertility[0] += mLandFertility[1];
    mLandFertility[1] = 0;

    for (int i = 2; i < 6; ++i) {
        mLandFertility[i-2] += mLandFertility[i];
        mLandFertility[i] = 0;
    }

    if (landToPlant > 0) {
        for (int i = 0; i < 5; ++i)
            mLandFertility[i+1] += landPlanted[i];
        mLandFertility[5] += landPlanted[5];
    }

    double workingCropYield = static_cast<double>(getRandomNumber(2)) + 3.0;

    if ((mCurrentYear % 7) == 0) {
        qtout << "Seven year locusts" << endl;
        workingCropYield *= 0.50;
    }

    double weightedCropYield = 0.0;
    int cropIndex = 1;

    for (int i = 0; i < 5; ++i, ++cropIndex)
        weightedCropYield += (static_cast<double>(landPlanted[i]) * (1.2 -
                              (0.2 * static_cast<double>(cropIndex))));

    if (mGrain[7] == 0) {
        mCropYield = 0.0;
        workingCropYield = 0.0;
    }
    else {
        mCropYield = static_cast<int>((workingCropYield * (weightedCropYield /
                                      static_cast<double>(mGrain[7]))) * 100.0) / 100.0;
        workingCropYield = mCropYield;
    }

    qtout << "Yield = " << workingCropYield << " HL./HA." << endl;

    int randomThree = getRandomNumber(3) + 3;
    if (randomThree < 9)
        return;

    int grainInfested = (randomThree * mTotalGrain) / 83;
    mGrain[4] = grainInfested * -1;
    mTotalGrain -= grainInfested;

    qtout << "Rats infest the granary" << endl;

    if (mTotalPeasants < 67 || mWarWithKingStatus == -2)
        return;

    int randomFour = getRandomNumber(4);
    if (randomFour > (mTotalPeasants / 30))
        return;

    qtout << "The king requires " << randomFour << " peasants for\n"
          << "his estate and mines. Will you supply\n"
          << "them (Y)es or pay " << (100 * randomFour) << " HL. of\n"
          << "grain instead (N)o ? " << flush;

    if (!getYesNo()) {
        int grainLevy = randomFour * 100;
        mGrain[9] = grainLevy * -1;
        mTotalGrain -= grainLevy;
    }
    else {
        mPeasants[2] = randomFour * -1;
        mTotalPeasants -= randomFour;
    }
}


/**
 * Sets up all variables for a new game.
 */
void Dukedom::initializeVars() {
    mCurrentYear = 0;
    mCropYield = INITIAL_CROP_YIELD;
    mTotalPeasants = 100;
    mTotalLand = 600;
    mTotalGrain = 4177;

    mDisaster  = 0;

    mPeasants[0] = 96;  // Peasants at start
    mPeasants[6] = -4;  // Natural deaths
    mPeasants[7] = 8;   // Births

    mLand[0] = 600;  // Land at start

    mGrain[0] = 5193;   // Grain at start
    mGrain[1] = -1344;  // Used for food
    mGrain[3] = -768;   // Seeding
    mGrain[7] = 1516;   // Crop yield
    mGrain[8] = -120;   // Castle expense
    mGrain[9] = -300;   // Royal tax

    mLandFertility[0] = 216;  // 100%
    mLandFertility[1] = 200;  // 80%
    mLandFertility[2] = 184;  // 60%

    mUnrest1 = 0;
    mUnrest2 = 0;
    mWarWithKingStatus = 0;

    mRandomValue[0] = initRandomNumber(4, 7);
    mRandomValue[1] = initRandomNumber(4, 8);
    mRandomValue[2] = initRandomNumber(4, 6);
    mRandomValue[3] = initRandomNumber(3, 8);
    mRandomValue[4] = initRandomNumber(5, 8);
    mRandomValue[5] = initRandomNumber(3, 6);
    mRandomValue[6] = initRandomNumber(3, 8);
    mRandomValue[7] = initRandomNumber(4, 8);
}


/**
 * Checks the the user can purchase land or plant farms with their current amount of grain.
 * @param purchasePrice the grain purchase price for new land or planted land
 */
void Dukedom::insufficientGrain(int purchasePrice) const {
    qtout << "But you don't have enough grain\n"
          << "You have " << mTotalGrain << " HL. of grain left," << endl;

    if (purchasePrice >= 4)
        qtout << "enough to buy " << (mTotalGrain / purchasePrice) << " HA. of land" << endl;
    else if (purchasePrice >= 1)
        qtout << "enough to plant " << (mTotalGrain / 2) << " HA. of land" << endl;
    else
        qtout << "all of which you will have to\ngive your peasants" << endl;
}


/**
 * The main structure of a year of gameplay. Calls each method in proper order and returns the
 * state of the current game.
 * @return boolean game state
 */
bool Dukedom::playOneYear() {
    printLastYearsResults();
    resetYearlyStats();

    if (endOfGameCondition())
        return true;

    if (feedPeasants())
        return true;

    buySellLand();

    if (mTotalLand < 10)
        if (totalLandCheck())
            return true;

    if (warWithKing())
        return true;

    grainProduction();

    if (updateWarStatus())
        if (warWithNeighbour())
            return true;

    updatePopulation();
    updateGrainTotal();

    return updateRoyalTax();
}


/**
 * Prints the data corresponding to the text in mGrainText for the current year.
 */
void Dukedom::printGrain() {
    qtout << endl;
    for (int i = 0; i < MAX_GRAIN; ++i)
        if (mGrain[i] != 0)
            qtout << formatString(mGrainText[i], 25) << formatInteger(mGrain[i], 5, false) << endl;

    qtout << endl
          << formatString("Grain at end of year", 25) << formatInteger(mTotalGrain, 5, false)
          << endl;
}


/**
 * Prints the data corresponding to the text in mLandText for the current year.
 */
void Dukedom::printLand() {
    qtout << endl;
    for (int i = 0; i < MAX_LAND; ++i)
        if (mLand[i] != 0 || i == 0)
            qtout << formatString(mLandText[i], 25) << formatInteger(mLand[i], 5, false) << endl;

    qtout << "\n 100%  80%  60%  40%  20% Depl\n";
    for (int i = 0; i < MAX_FERTILITY; ++i)
        qtout << formatInteger(mLandFertility[i], 5, true);

    qtout << endl;
}


/**
 * Prints last years data for the user to make decisions for the new year.
 */
void Dukedom::printLastYearsResults() {
    printTotals();
    if (mSkipDetailedReports)
        return;

    printPeasants();
    printLand();
    printGrain();

    if (mCurrentYear <= 0)
        qtout << "(Severe crop damage due to seven\n"
              << " year locusts)" << endl;

    qtout << endl << endl;
}


/**
 * Prints the data corresponding to the text in mPeasantText for the current year.
 */
void Dukedom::printPeasants() {
    qtout << endl;
    for (int i = 0; i < MAX_PEASANTS; ++i)
        if (mPeasants[i] != 0 || i == 0)
            qtout << formatString(mPeasantsText[i], 25) << formatInteger(mPeasants[i], 5, false)
                  << endl;

    qtout << formatString("Peasants at end", 25) << formatInteger(mTotalPeasants, 5, false) << endl;
}


/**
 * Prints the current years totals for the users land and peasants.
 */
void Dukedom::printTotals() const {
    qtout << "\n\n\n"
          << "Year " << mCurrentYear << " Peasants " << mTotalPeasants << " Land " << mTotalLand
          << " Grain " << mTotalGrain << endl;
}


/**
 * Prints testing variables. Not used but kept around because it's in the orignal code.
 */
void Dukedom::printVars() const {
    qtout << "mSkipDetailedReports = " << mSkipDetailedReports << endl
          << "mCropYield = " << mCropYield << endl
          << "mDisaster  = " << mDisaster  << endl
          << "mUnrest1 = " << mUnrest1 << endl
          << "mUnrest2 = " << mUnrest2 << endl
          << "mWarWithKingStatus = " << mWarWithKingStatus << endl;
}


/**
 * Resets all yearly record variables for a new year.
 */
void Dukedom::resetYearlyStats() {
    ++mCurrentYear;

    for (int i = 0; i < MAX_PEASANTS; ++i)
        mPeasants[i] = 0;

    for (int i = 0; i < MAX_LAND; ++i)
        mLand[i] = 0;

    for (int i = 0; i < MAX_GRAIN; ++i)
        mGrain[i] = 0;

    mPeasants[0] = mTotalPeasants;
    mLand[0] = mTotalLand;
    mGrain[0] = mTotalGrain;
}


/**
 * Save end game stats to a file.
 */
void Dukedom::saveScore() {
    QFile scoresFile("dukedom_scores.txt");
    if (!scoresFile.open(QIODevice::Append))
        return;

//    QString gameOverMsg = (type = 1 ? "WIN : " : "LOSE: ");
    QTextStream fout(&scoresFile);
    fout << QString("Year %1 - %2 peasants - %3 land - %4 grain\n")
            .arg(mCurrentYear).arg(mTotalPeasants).arg(mTotalLand).arg(mTotalGrain);
}


/**
 * Checks the current amount of grain left for a game over condition.
 * @return boolean game over condition
 */
bool Dukedom::totalGrainCheck() const {
    if (mTotalGrain >= 429)
        return false;

    qtout << "You have so little grain left that\n"
          << "the High King has abolished your Ducal\n"
          << "right" << endl;

    return true;
}


/**
 * Checks the current amount of land left for a game over condition.
 * @return boolean game over condition
 */
bool Dukedom::totalLandCheck() const {
    if (mTotalLand >= 198)
        return false;

    qtout << "You have so little land left that\n"
          << "the High King has abolished your Ducal\n"
          << "right" << endl;

    return true;
}


/**
 * Checks the current peasant count for a game over condition.
 * @return boolean game over condition
 */
bool Dukedom::totalPeasantCheck() const {
    if (mTotalPeasants >= 33)
        return false;

    qtout << "You have so few peasants left that\n"
          << "the High King has abolisted your Ducal\n"
          << "right" << endl;

    return true;
}


/**
 * Checks current unrest values for a game over condition.
 * @return boolean game over condition
 */
bool Dukedom::unrestCheck() const {
    if (mUnrest1 > 88 || mUnrest2 > 99) {
        qtout << "The peasants tire of war and starvation\n"
              << "You are deposed" << endl;
        return true;
    }

    return false;
}


/**
 * Handles grain calculations for the end of the year depending on grain planted and decay.
 */
void Dukedom::updateGrainTotal() {
    mGrain[7] = static_cast<int>(mCropYield * mGrain[7]);
    mTotalGrain += mGrain[7];
    int x1 = mGrain[7] - 4000;

    if (x1 > 0)
        mGrain[8] = (-1 * static_cast<int>(0.1 * x1));

    mGrain[8] -= 120;
    mTotalGrain += mGrain[8];
}


/**
 * Updates the current population at the end of a year and handles distaster states.
 */
void Dukedom::updatePopulation() {
    double x1 = getRandomNumber(7);
    int x2;

    if (static_cast<int>(x1) <= 3) {
        if (static_cast<int>(x1) != 1) {
            qtout << "A POX EPIDEMIC has broken out" << endl;  // FUCK THIS SHIT ALWAYS HAPPENS TO ME

            x2 = static_cast<int>(x1 * 5.0);
            mPeasants[5] = (-1 * static_cast<int>(mTotalPeasants / x2));
            mTotalPeasants += mPeasants[5];
        }
        else {
            qtout << "D = " << mDisaster  << endl;

            if (mDisaster  <= 0) {
                qtout << "THE BLACK PLAGUE has struck the area" << endl;

                mDisaster = 13;
                x2 = 3;
                mPeasants[5] = (-1 * static_cast<int>(mTotalPeasants / x2));
                mTotalPeasants += mPeasants[5];
            }
        }
    }

    x1 = getRandomNumber(8) + 4;
    if (mPeasants[4] != 0)
        x1 = 4.5;

    mPeasants[7] = static_cast<int>(mTotalPeasants / x1);
    mPeasants[6] = static_cast<int>(0.3 - (mTotalPeasants / 22));
    mTotalPeasants += mPeasants[6] + mPeasants[7];
    --mDisaster ;
}


/**
 * Handles royal tax at the end of each year and returns a game over state if the user doesn't have
 * enough grain to pay.
 * @return boolean game over state
 */
bool Dukedom::updateRoyalTax() {
    if (mWarWithKingStatus >= 0) {
        int x1 = (-1 * static_cast<int>(mTotalLand / 2));

        if (mWarWithKingStatus >= 2)
            x1 *= 2;

        if ((-1 * x1) > mTotalGrain) {
            qtout << "You have insufficient grain to pay\n"
                  << "the royal tax" << endl;

            return true;
        }

        mGrain[9] += x1;
        mTotalGrain += x1;
    }

    mUnrest2 = static_cast<int>(mUnrest2 * 0.85) + mUnrest1;

    return false;
}


/**
 * Updates the current war state with the king and returns a war status as the user can only be at
 * war with the king or it's neighbours, not both.
 * @return boolean war status
 */
bool Dukedom::updateWarStatus() {
    if (mWarWithKingStatus != -1)
        return true;

    qtout << "The High King calls for peasant levies\n"
          << "and hires many foreign mercenaries" << endl;

    mWarWithKingStatus = -2;
    return false;
}


/**
 * Handles direct war with the king and returns a game over condition depending on how it ends.
 * @return boolean game over condition
 */
bool Dukedom::warWithKing() const {
    if (mWarWithKingStatus != 2)
        return false;

    qtout << "The king's army is about to attack\n"
          << "your duchy" << endl;

    int mercenariesHired = mTotalGrain / 100;

    qtout << "You have hired " << mercenariesHired << " foreign mercenaries\n"
          << "at 100 HL. each (pay in advance)" << endl;

    if (((8 * mercenariesHired) + mTotalPeasants) > 2399)
        qtout << "Wipe the blood from the crown - you\n"
              << "are High King! A nearby monarchy\n"
              << "THREATENS WAR; HOW MANY ........" << endl;
    else
        qtout << "Your head is placed atop the\n"
              << "castle gate." << endl;

    return true;
}


/**
 * Handles both the kings suspicions and war depending on the current war state and the users grain
 * wealth. This will return a game over condition.
 * @return boolean game over condition
 */
bool Dukedom::warWithNeighbour() {
    int x1 = static_cast<int>(11.0 - 1.5 * mCropYield);
    int x2 = 0;

    if (x1 < 2)
        x1 = 2;

    if (mWarWithKingStatus != 0 || mTotalPeasants <= 109 ||
        ((17 * (mTotalLand - 400)) + mTotalGrain) <= 10600)
        x2 = 0;
    else {
        qtout << "The High King grows uneasy and may\n"
              << "be subsidizing wars against you" << endl;

        x1 += 2;
        x2 = mCurrentYear + 5;
    }

    int x3 = getRandomNumber(5);
    if (x3 > x1)
        return false;

    qtout << "A nearby Duke threatens war; " << endl;

    x2 = static_cast<int>(x2 + 85 + 18 * getRandomNumber(7));
    double x4 = 1.2 - (static_cast<double>(mUnrest1) / 16.0);
    int x5 = static_cast<int>(static_cast<double>(mTotalPeasants) * x4) + 13;

    qtout << "Will you attack first ? " << flush;
    bool ans = getYesNo();
    int warCasualties;

    if (!ans) {
        if (x2 >= x5) {
            qtout << "Peace negotiations failed - you\n"
                  << "need professionals" << endl;

            warCasualties = x3 + x1 + 2;
            mPeasants[3] = warCasualties * -1;
            x2 += 3 * mPeasants[3];
        }
        else {
            qtout << "Peace negotiations successful" << endl;

            warCasualties = x1 + 1;
            mPeasants[3] = warCasualties * -1;
            x2 = 0;
        }

        mTotalPeasants -= warCasualties;

        if (x2 < 1) {
            mUnrest1 -= (2 * mPeasants[2]) - (3 * mPeasants[4]);
            return false;
        }
    }

    int mercenariesHired = 0;

    while (true) {
        qtout << "How many mercenaries will you hire\n"
              << "at 40 HL. each = " << flush;
        mercenariesHired = getInteger();

        if (mercenariesHired > 75) {
            qtout << "There are only 75 available for hire" << endl;
            continue;
        }

        break;
    }

    x2 = static_cast<int>(static_cast<double>(x2) * WAR_FACTOR);
    x5 = static_cast<int>(static_cast<double>(mTotalPeasants) * x4) +
         (7 * mercenariesHired) + 13;
    int x6 = x2 - (4 * mercenariesHired) - (static_cast<int>(0.25 * static_cast<double>(x5)));

    x2 = x5 - x2;
    mLand[2] = static_cast<int>(0.8 * static_cast<double>(x2));

    if (static_cast<double>(mLand[2] * -1) > (static_cast<double>(0.67 * mTotalLand))) {
        qtout << "You have been overrun and have lost\n"
              << "your entire dukedom" << endl;
        return true;
    }

    x1 = mLand[1];
    int landIndex = 3;

    for (int i = 0; i < 3; ++i, --landIndex) {
        x3 = static_cast<int>(x1 / landIndex);
        if ((x3 * -1) <= mLandFertility[i])
            x5 = x3;
        else
            x5 = mLandFertility[i];

        mLandFertility[i] += x5;
        x1 -= x5;
    }

    if (mLand[2] >= 399) {
        qtout << "You have overrun the enemy and annexed\n"
              << "his entire dukedom" << endl;

        mGrain[6] = 3513;
        mTotalGrain += mGrain[6];
        x6 = -47;
        x4 = 0.55;

        if (mWarWithKingStatus <= 0) {
            mWarWithKingStatus = 1;
            qtout << "The king fears for his throne and\n"
                  << "may be planning direct action" << endl;
        }
    }
    else {
        if (x2 >= 1) {
            qtout << "You have won the war" << endl;
            x4 = 0.67;
            mGrain[6] = static_cast<int>(1.7 * mLand[2]);
            mTotalGrain += mGrain[6];
        }
        else {
            qtout << "You have lost the war" << endl;
            x4 = mGrain[7] / mTotalLand;
        }

        if (x6 <= 9)
            x6 = 0;
        else
            x6 = static_cast<int>(x6 / 10);
    }

    if (x6 > mTotalPeasants)
        x6 = mTotalPeasants;

    mPeasants[3] -= x6;
    mTotalPeasants -= x6;
    mGrain[7] += static_cast<int>(x4 * static_cast<double>(mLand[2]));
    x6 = 40 * mercenariesHired;

    if (x6 <= mTotalGrain)
        mGrain[5] = x6 * -1;
    else {
        mGrain[5] = mTotalGrain * -1;
        mPeasants[4] = (-1 * static_cast<int>((x6 - mTotalGrain) / 7) - 1);

        qtout << "There isn't enough grain to pay the\n"
              << "mercenaries" << endl;
    }

    mTotalGrain += mGrain[5];

    if ((mPeasants[4] * -1) > mTotalPeasants)
        mPeasants[4] = mTotalPeasants * -1;

    mTotalPeasants += mPeasants[4];
    mTotalLand += mLand[2];
    mUnrest1 -= (2 * mPeasants[3]) - (3 * mPeasants[4]);

    return false;
}


/* ********** Private Methods ********** */
/**
 * Formats output before being displasyed to the user.
 * @param intVal value to format
 * @param fieldSize amount of spaces to format with
 * @param rightJustify if the text should be right justified
 * @return formatted QString
 */
QString Dukedom::formatInteger(int intVal, int fieldSize, bool rightJustify) {
    QString spaces;
    QString intStr = QString("%1").arg(intVal);

    for (int i = 0; i < fieldSize; ++i)
        spaces.append(" ");

    if (rightJustify)
        return QString("%1%2").arg(spaces.left(fieldSize - intStr.length())).arg(intStr);
    else
        if (intVal < 0)
            return intStr;
        else
            return QString(" %1").arg(intStr);
}


/**
 * Formats a QString based on string given.
 * @param strVal string to format
 * @param fieldSize amount of spaces to format with
 * @return  formatted QString
 */
QString Dukedom::formatString(QString strVal, int fieldSize) {
    QString spaces;

    for (int i = 0; i < fieldSize; ++i)
        spaces.append(" ");

    return QString("%1%2").arg(strVal).arg(spaces.left(fieldSize - strVal.length()));
}


/**
 * Get an integer from the user. This is only for use with a CLI.
 * @return integer from user
 */
int Dukedom::getInteger() {
    QString input;
    bool isNumeric;
    int val;

    while (true) {
        qtin >> input;

        if (!input.isEmpty() && input.length() > 0) {
            val = input.toInt(&isNumeric);

            if (!isNumeric)
                qtout << "Please enter a number : " << flush;
            else if (val < 0)
                qtout << "Please enter a non-negative number : " << flush;
            else
                break;
        }
    }

    if (input.isEmpty() || input.length() == 0)
        val = 0;

    return val;
}


/**
 * Generate a random value based on the value in mRandomValue at the given position.
 * @param pos position in mRandomValue
 * @return random value
 */
int Dukedom::getRandomNumber(int pos) {
    int rtnVal;
    int randomOne;

    randomOne = getRandomNumber(-RANDOM_RANGE, RANDOM_RANGE);
    rtnVal = randomOne + mRandomValue[pos-1];

    return rtnVal;
}


/**
 * Generate a random value between given min and max values/
 * @param min minimum value
 * @param max maximum value
 * @return random value
 */
int Dukedom::getRandomNumber(int min, int max) {
    qsrand(QTime::currentTime().msec());

    return ((qrand() % (max - min + 1)) + min);
}


/**
 * Prompt the user for a yes or no answer. This is only for use with a CLI.
 * @return string from user
 */
bool Dukedom::getYesNo() {
    QString input;

    while (true) {
        qtin >> input;

        if (input.isEmpty() || input.length() == 0) {
            qtout << "Please answer [y]es or [n]o : " << flush;
            continue;
        }

        input = input.toUpper();
        if (input[0] == 'Y')
            return true;
        else if (input[0] == 'N')
            return false;
        else
            qtout << "Please answer [y]es or [n]o : " << flush;
    }
}


/**
 * Generate a random with certain conditions applied to it.
 * @param min minimum value
 * @param max maximum value
 * @return random value
 */
int Dukedom::initRandomNumber(int min, int max) {
    int rtnVal;
    int randomOne;
    int randomTwo;

    randomOne = getRandomNumber(min, max);
    randomTwo = getRandomNumber(min, max);

    if (randomTwo > 5) {
        int randomThree = getRandomNumber(min, max);
        rtnVal = (randomOne + randomThree) / 2;
    }
    else
        rtnVal = randomOne;

    return rtnVal;
}

#-------------------------------------------------
#
# Project created by QtCreator 2013-10-24T09:37:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QDukedom
TEMPLATE = app
CONFIG += console


SOURCES += main.cpp\
           dukedom.cpp \
    mainwindow.cpp

HEADERS  += dukedom.h \
    mainwindow.h

FORMS    +=

RESOURCES += \
    resources.qrc

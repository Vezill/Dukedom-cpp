/* mainwindow.h
 *
 * Dukedom Game Class
 * Version: 0.4
 * Date: November 20, 2013
 * Author: Nick Donais
 * Email: dona0078@algonquinlive.com
 * SID: 040596989
 * Course: CST8333 Advanced Languages
 * Lab Section: 410
 * Professor: Stan Pieda
 *
 * This is the main window setup for QDukedom. Although this was the original plan, this was
 * abandoned half way through when I realized just how much of a code rewite I'd have to do to the
 * Dukedom class to get this to work the way I wanted that I decided to do something else for the
 * requirements.
 */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QApplication>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QRadioButton>
#include <QtGui/QTextEdit>


class MainWindow : public QMainWindow {
    Q_OBJECT

    /* Year display widget. */
    QLabel *uiYearLabel;
    /* Yearly data display widget. */
    QTextEdit *uiYearDataTextEdit;
    /* Stage description widget. */
    QLabel *uiStageDescLabel;
    /* Stage controls widget. */
    QWidget *uiStageControlsWidget;

    /* Layout for feed peasants stage. */
    QHBoxLayout *uiFeedPeasantsLayout;
    /* Line edit for amount of grain to feed peasants. */
    QLineEdit *uiGrainAmountLineEdit;

    /* Layout for buy/sell land stage. */
    QGridLayout *uiLandLayout;
    /* Radio button to find out if user is buying or selling land. */
    QRadioButton *uiBuyLandRadioButton;
    /* Line edit for amount of land to buy/sell. */
    QLineEdit *uiLandAmountLineEdit;

    /* Layout for war stage. */
    QGridLayout *uiWarLayout;
    /* Radio button to find out if user attacks first or not. */
    QRadioButton *uiAttackRadioButton;
    /* Line edit for amount of mercenaries to send. */
    QLineEdit *uiMercAmountLineEdit;

    /* Layout for year end stage. */
    QHBoxLayout *uiYearEndLayout;

public:
    MainWindow(QWidget *parent=0);

    void stageFeedPeasants();
    void stageBuySellLand();
    void stageWar();
    void stageYearEnd();

public slots:
    void reset();

    void handleFeedPeasants();
    void handleBuySellLand();
    void handleWar();
    void handleYearEnd();
};

#endif // MAINWINDOW_H

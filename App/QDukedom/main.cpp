/* main.cpp
 *
 * Dukedom main
 * Version: 1.0 - cli
 * Date: October 17, 2013
 * Author: Nick Donais
 * Email: dona0078@algonquinlive.com
 * SID: 040596989
 * Course: CST8333 Advanced Languages
 * Lab Section: 410
 * Professor: Stan Pieda
 */
#include "dukedom.h"
#include <QtCore/QCoreApplication>

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);

    Dukedom game;
    game.gameLoop();

    return EXIT_SUCCESS;
}
